#!/usr/bin/env python3

# generate-mappings.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gzip
import json
import argparse
from typing import List, Optional
from pathlib import Path
from requests import get, Response
from time import time
from os import makedirs, getenv
from tqdm import tqdm
from datetime import datetime, timedelta

DAILY_EXPORT_URL = f'http://files.tmdb.org/p/exports/tv_series_ids_{datetime.now().strftime("%m_%d_%Y")}.json.gz'
EXPORT_ARCHIVE = 'tv_series_ids.json.gz'
SHOW_DATA_DIR = 'data/show'
SHOW_KG_DIR = 'kg/show'
MAPPINGS_DIR = 'mappings/show/generated'
API_BASE_URL = 'https://api.themoviedb.org/3'
API_KEY = getenv('TMDB_API_KEY')
LANGUAGE = 'en-US'
SHOW_MAPPING_TEMPLATE = 'mappings/show/show_template.rml.ttl'


def get_from_api(url: str, params: dict) -> dict:
    for i in range(0, 5):
        try:
            r = get(url, params = params)
            if r.status_code != 200:
                print(f'Request failed: HTTP {r.status_code} for show {show_id}')
                return None
            break
        except ConnectionResetError:
            print('Connection Reset received, sleeping for 5s')
            time.sleep(5)
    return r.json()

def generate_mapping(show_id: int) -> Optional[Path]:
    """
    Generate RML mapping rules by filling in the mapping templates
    with data from the TMDB API.
    """
    show: dict = {}
    show_title: str
    number_of_seasons: int
    number_of_episodes: int
    data: dict
    r: Response
    path: Path = Path(MAPPINGS_DIR).joinpath(f'{show_id}.rml.ttl')
    
    # Get show data from API
    data = get_from_api(f'{API_BASE_URL}/tv/{show_id}',
            {'api_key': API_KEY, 'language': LANGUAGE, 'append_to_response': 'external_ids'})

    if data is None:
        return

    # Copy show properties
    show['id'] = data['id']
    show['original_name'] = data['original_name']
    show['overview'] = data.get('overview')
    show['poster_path'] = data.get('poster_path')
    show['first_air_date'] = data['first_air_date']
    if 'external_ids' in data:
        show['imdb_id'] = data['external_ids'].get('imdb_id')
        show['tvdb_id'] = data['external_ids'].get('tvdb_id')
    show['seasons'] = []

    # Seasons data
    for season in data.get('seasons', []):
        season_id = season['id']
        season_number = season['season_number']

        # Get episodes data from API
        data = get_from_api(f'{API_BASE_URL}/tv/{show_id}/season/{season_number}',
                            {'api_key': API_KEY, 'language': LANGUAGE})

        # Episode data
        episodes = []
        if data is not None:
            for episode in data.get('episodes', []):
                episode_number = episode['episode_number']
                episodes.append({
                    'id': episode['id'],
                    'season_id': season_id,
                    'name': episode.get('name'),
                    'overview': episode.get('overview'),
                    'air_date': episode.get('air_date'),
                    'episode_number': episode.get('episode_number')
                })

        # Add season
        show['seasons'].append({
            'id': season_id,
            'name': season.get('name'),
            'season_number': season_number,
            'overview': season.get('overview'),
            'air_date': season.get('air_date'),
            'episodes': episodes
        })

    # Update data only if necessary
    data_path = Path(SHOW_DATA_DIR).joinpath(f'{show_id}.json')
    update_required = False
    if data_path.exists():
        with open(data_path, 'r') as f:
            existing_data = json.load(f)
            if existing_data != show:
                update_required = True

                # Data updated, regenerate KG for show
                kg = Path(SHOW_KG_DIR).joinpath(f'{show_id}.gz')
                if kg.exists():
                    kg.unlink()
    # New show, fully generate
    else:
        update_required = True

    if update_required:
        # Store new JSON data
        with open(data_path, 'w') as f:
            json.dump(show, f)

        # Generate RML mapping rules from template
        with open(SHOW_MAPPING_TEMPLATE, 'r') as f1:
            with open(str(path), 'w') as f2:
                for l in f1.readlines():
                    if '$SHOW_ID' in l:
                        l = l.replace('$SHOW_ID', str(show_id))
                    f2.write(l)

    return path

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate RML mapping rules for shows from The Movie DB API')
    parser.add_argument('--full', action='store_true', help='Generate from scratch instead of incremental changes')
    parser.add_argument('--cache', action='store_true', help='Enable HTTP cache, requires requests-cache')
    args = parser.parse_args()

    if API_KEY is None:
        print('TMDB API key is missing! Provide your API key as an environment variable "TMDB_API_KEY"')
        sys.exit(1)

    if args.cache:
        import requests_cache
        print('HTTP cache enabled')
        requests_cache.install_cache('http_cache')

    print('Generating RDF knowledge graph from The Movie DB')

    # Retrieve list of show ids
    show_ids: List = []
    response: Response

    if args.full:
        print('Generating from scratch...')
        response = get(DAILY_EXPORT_URL, stream=True)
        with open(EXPORT_ARCHIVE, 'wb') as f:
            for chunk in response:
                f.write(chunk)

        with gzip.open(EXPORT_ARCHIVE, 'r') as f:
            for line in f:
                s = json.loads(line)
                show_ids.append(s['id'])
        print(f'{len(show_ids)} shows in dataset')
    else:
        yesterday = datetime.now() - timedelta(days=1)
        print(f'Generating from changes API since {yesterday.strftime("%d-%m-%Y")} in incremental mode...')
        response = get(f'{API_BASE_URL}/tv/changes',
                       params = {'api_key': API_KEY, 'start_date': yesterday.strftime('%d-%m-%Y')})
        response.raise_for_status()
        data = response.json()

        # Paging is disabled for now, but we have to detect it when this behavior is changed
        assert(data['total_pages'] == 1)

        for changed_show in data['results']:
            # API returns non-existing show IDs when 'adult': null
            if changed_show['adult'] is not None:
                show_ids.append(changed_show['id'])
        print(f'{len(show_ids)} shows are updated since last 24 hours')

    # Create data dirs
    makedirs(SHOW_DATA_DIR, exist_ok=True)
    makedirs(SHOW_KG_DIR, exist_ok=True)
    makedirs(MAPPINGS_DIR, exist_ok=True)

    # Generate mappings
    for show_id in tqdm(show_ids, total=len(show_ids)):
        generate_mapping(show_id)
