#!/usr/bin/env python3

# execute-mappings.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from multiprocessing import Pool
from tqdm import tqdm
from pathlib import Path
from tempfile import NamedTemporaryFile
from os import makedirs

from mapper import Mapper

MAPPER_JAR_PATH = Path(NamedTemporaryFile().name)
MAPPINGS_DIR = 'mappings/show/generated'
SHOW_KG_DIR = 'kg/show'
NUMBER_OF_PROCESSES = 3


def execute_mapping(mapping_file: Path):
    """
    Execution function for Multiprocessing Pool.
    """
    m: Mapper = Mapper(MAPPER_JAR_PATH)
    m.execute(mapping_file)
    return mapping_file

if __name__ == '__main__':
    p: Pool = Pool(processes=NUMBER_OF_PROCESSES)
    makedirs(SHOW_KG_DIR, exist_ok=True)

    print('Generating RDF knowledge graph from generated mapping files')

    mapping_files = list(Path(MAPPINGS_DIR).glob('*.rml.ttl'))
    processed_files = list(Path(SHOW_KG_DIR).glob('*'))
    unprocessed_files = []

    for f1 in mapping_files:
        path = Path(SHOW_KG_DIR).joinpath(f1.name.replace('.rml.ttl', '.gz'))
        if not path.exists():
            unprocessed_files.append(f1)

    print(f'Skipped {len(mapping_files) - len(unprocessed_files)} files, already processed')

    for f in tqdm(p.imap_unordered(execute_mapping, unprocessed_files),
                  total=len(unprocessed_files),
                  miniters=1):
        pass
